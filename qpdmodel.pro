include(src/handlers/handlers.pri)
include(src/model/model.pri)
include(src/widgets/widgets.pri)

TEMPLATE	= app
TARGET		= qpdmodel

QT			+= core gui

HEADERS		+= qpdmodel.h \
			+= properties.h
SOURCES		+= main.cpp \
			qpdmodel.cpp
