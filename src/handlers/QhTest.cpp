/*
 * Copyright (c) 2013, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "QhTest.h"
#include <properties.h>

#define HANDLER_NAME	"handler.test"

class QpTest0: public QmProperty
{
	Q_OBJECT

public:
	QpTest0(const QmId id = PROPERTY_INVALID) : QmProperty(id)
	{
		Q_ASSERT(id != PROPERTY_INVALID);
		briefDescription = "Test property 0";
		description = "This is test ptoperty #0";
		QList<QVariant> values;
		values 	<< 0
				<< 1
				<< 2
				<<3;
		setOwnValueList(values);
		setOwnValue(getOwnValueList().first());
		dependences << PROPERTY_1;
	}

	void updateValue(void)
	{
		Q_CHECK_PTR(getModelValue);
		QVariant p1 = getModelValue(PROPERTY_1, QP_PROPERTY_ROLE_RAW);
		if (p1.toBool())
			setOwnValue(getOwnValueList().last());
		else
			setOwnValue(getOwnValueList().first());
		updateModelValue(id, getOwnValue());
	}
};

class QpTest1: public QmProperty
{
	Q_OBJECT

public:
	QpTest1(const QmId id = PROPERTY_INVALID) : QmProperty(id)
	{
		Q_ASSERT(id != PROPERTY_INVALID);
		briefDescription = "Test property 1";
		description = "This is test ptoperty #1";
		QList<QVariant> values;
		values 	<< true
				<< false;
		setOwnValueList(values);
		setOwnValue(getOwnValueList().first());
		dependences << PROPERTY_0;
	}

	QVariant getValueDisplay(const QVariant v) {
		if (v.toBool())
			return QVariant("True!");
		else
			return QVariant("False...");
	}
};

#include "QhTest.moc"

QhTest::QhTest(int instance)
	: QmHandler(instance)
{
	property0 = new QpTest0(PROPERTY_0);
	property1 = new QpTest1(PROPERTY_1);

	properties 	<< property0
				<< property1;

	name = QString(HANDLER_NAME);
}

QhTest::~QhTest() {
	delete property0;
	delete property1;
}

