/*
 * Copyright (c) 2012, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef QMDEBUG_H_
#define QMDEBUG_H_

#include <QObject>

class QmDebug : public QObject
{
	Q_OBJECT

public:
	typedef enum {
		None,
		Error,
		Warning,
		Info
	} QmDebugLevel;

	QmDebug(QObject* parent=0, const QmDebugLevel level = None, const QString moduleName = QString(), const QString className = QString());

	void			setLevel(const QmDebugLevel level);
	void			setModuleName(const QString moduleName = 0);
	void			setClassName(const QString className = 0);
	void			error(const QString file = 0, const int line = 0, const QString message = 0);
	void			warning(const QString file = 0, const int line = 0, const QString message = 0);
	void			info(const QString file = 0, const int line = 0, const QString message = 0);

private:
	void			print(const QmDebugLevel level = None, const QString file = QString(), const int line = 0, const QString message = QString());
	QObject*		parent;
	QmDebugLevel	level;
	QString			className;
	QString			moduleName;
};

#endif /* QMDEBUG_H_ */
