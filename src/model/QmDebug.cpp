/*
 * Copyright (c) 2012, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <QDateTime>
#include "QmDebug.h"

QmDebug::QmDebug(QObject* parent, const QmDebugLevel level, const QString moduleName, const QString className)
{
	this->parent = parent;
	this->level = level;
	this->moduleName = moduleName;
	this->className = className;
}

void
QmDebug::setLevel(const QmDebugLevel level)
{
	this->level = level;
}

void
QmDebug::setModuleName(const QString moduleName)
{
	Q_ASSERT(moduleName.isNull());
	this->moduleName = moduleName;
}

void
QmDebug::setClassName(const QString className)
{
	Q_ASSERT(className.isNull());
	this->className = className;
}

void
QmDebug::error(const QString file, const int line, const QString message)
{
	print(Error, file, line, message);
	return;
}

void
QmDebug::warning(const QString file, const int line, const QString message)
{
	print(Warning, file, line, message);
	return;
}

void
QmDebug::info(const QString file, const int line, const QString message)
{
	print(Info, file, line, message);
	return;
}

void
QmDebug::print(const QmDebugLevel level, const QString file, const int line, const QString message)
{
#ifdef QT_DEBUG
	if (this->level < level)
			return;

	QString prefix;

	Q_ASSERT((level == Error) ||
			(level == Warning) ||
			(level == Info));

	switch (level) {
		case Error:
			prefix = QString(" [ERR] ");
			break;

		case Warning:
			prefix = QString(" [WRN] ");
			break;

		case Info:
			prefix = QString(" [INF] ");
			break;

		default:
			break;
	}

	QDateTime current = QDateTime::currentDateTime();
	QString time = current.toString("hh:mm:ss");

	QString text = time + prefix;
	if (!className.isEmpty())
		text += QString(" ") + className + QString(" ");

	text += file;

	/* Assuming no filename/line is set, only message in 1st parameter */
	if ((line != 0) || message.isEmpty())
		text+= QString(" @") + QString::number(line) + QString(": ");

	text += message;
	std::cout << text.toStdString() << std::endl;
#endif // QT_DEBUG
	return;
}

