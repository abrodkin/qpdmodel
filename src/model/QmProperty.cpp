/*
 * Copyright (c) 2012, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "QmProperty.h"

#define DEBUG_LEVEL		QmDebug::Info

QmProperty::QmProperty(const QmId id) {
	this->id = id;
	debug = new QmDebug(this, DEBUG_LEVEL, metaObject()->className());
	return;
}

QmProperty::~QmProperty() {
	delete debug;
}

QList<QmId>
QmProperty::getDependences(void)
{
	return dependences;
}

QVariant
QmProperty::getValue(const QmPropertyRole r){
	QVariant result;
	Q_ASSERT((r == QP_PROPERTY_ROLE_RAW) ||
			(r == QP_PROPERTY_ROLE_DISPLAY) ||
			(r == QP_PROPERTY_ROLE_DESCRIPTION) ||
			(r == QP_PROPERTY_ROLE_BRIEF_DESCRIPTION) ||
			(r == QP_PROPERTY_ROLE_ID));

	switch (r) {
		case QP_PROPERTY_ROLE_RAW:
			result = getOwnValue();
			break;

		case QP_PROPERTY_ROLE_DISPLAY:
			result = getValueDisplay(getOwnValue());
			break;

		case QP_PROPERTY_ROLE_DESCRIPTION:
			result = getValueDescription();
			break;

		case QP_PROPERTY_ROLE_BRIEF_DESCRIPTION:
			result = getValueBriefDescription();
			break;

		case QP_PROPERTY_ROLE_ID:
			result = QVariant(id);
			break;

		default:
			break;
	}
	debug->info(__FILE__, __LINE__, QString("getValue (role %1) of <%2>: %3").arg(r).arg(getValueBriefDescription().toString()).arg(result.toString()));
	return result;
}

QList<QVariant>
QmProperty::getValueList(const QmPropertyRole r)
{
	QList<QVariant> result;
	QList<QVariant> raw;
	Q_ASSERT((r == QP_PROPERTY_ROLE_RAW) ||
			(r == QP_PROPERTY_ROLE_DISPLAY));

	debug->info(__FILE__, __LINE__, QString("getValueList (role %1) of <%2>:").arg(r).arg(getValueBriefDescription().toString()));

	raw = getOwnValueList();

	switch (r) {
		case QP_PROPERTY_ROLE_RAW:
			foreach (QVariant v, raw) {
				result << v;
				debug->info(__FILE__, __LINE__, QString("\t<%1>").arg(v.toString()));
			}
			break;

		case QP_PROPERTY_ROLE_DISPLAY:
			foreach (QVariant v, raw) {
				result << getValueDisplay(v);
				debug->info(__FILE__, __LINE__, QString("\t<%1>").arg(getValueDisplay(v).toString()));
			}
			break;

		default:
			break;
	}
	return result;
}

void
QmProperty::setValue(const QVariant v)
{
	Q_ASSERT(!v.isNull());
	if (getOwnValueList().isEmpty() || getOwnValueList().contains(v)) {
		setOwnValue(v);
		Q_CHECK_PTR(updateModelValue);
		updateModelValue(id, v);
		debug->info(__FILE__, __LINE__, QString("setValue of <%1>: %2").arg(getValueBriefDescription().toString()).arg(v.toString()));
	}
	return;
}

QVariant
QmProperty::getValueDisplay(const QVariant v)
{
	Q_ASSERT(!v.isNull());
	return v;
}

QVariant
QmProperty::getValueDescription(const QVariant)
{
	return QVariant(description);
}

QVariant
QmProperty::getValueBriefDescription(const QVariant)
{
	return QVariant(briefDescription);
}

bool
QmProperty::leadingNumberLessThan(const QVariant q1, const QVariant q2)
{
	Q_ASSERT(!q1.isNull());
	Q_ASSERT(!q2.isNull());
	QString s1 = q1.toString().section(". ", 0, 0);
	QString s2 = q2.toString().section(". ", 0, 0);
	return s1.toFloat() < s2.toFloat();
}

QVariant
QmProperty::getOwnValue(void)
{
	return value;
}

QList<QVariant>
QmProperty::getOwnValueList(void)
{
	return valueList;
}

void
QmProperty::setOwnValue(const QVariant v)
{
	Q_ASSERT(!v.isNull());
	value = v;
}

void
QmProperty::setOwnValueList(const QList<QVariant> v)
{
	valueList = v;
}
