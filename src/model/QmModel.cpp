/*
 * Copyright (c) 2012, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "QmModel.h"

#define DEBUG_LEVEL		QmDebug::Info

QmModel::QmModel() {
	debug = new QmDebug(this, DEBUG_LEVEL, metaObject()->className());
	properties = new QHash<QmId, QmProperty*>;
	dependencies = new QHash<QmId, QList<QmProperty*>* >;
	internalQueue = new QList<QPair<QmId, QVariant> >;
}

QmModel::~QmModel() {
	delete debug;
	delete properties;
	delete dependencies;
	delete internalQueue;
}

void
QmModel::addHandler(QmHandler* property)
{
	Q_ASSERT(property);
	QList<QmProperty *> props;
	debug->info(__FILE__, __LINE__, QString("Adding handler <%1> for properties:").arg(property->getName()));
	props = property->getProperties();
	foreach (QmProperty* p, props) {
		/* Setting call-backs */
		p->setCallback_getModelValue(std::tr1::bind(&QmModel::getProperty, this, std::tr1::placeholders::_1, std::tr1::placeholders::_2));
		p->setCallback_setModelValue(std::tr1::bind(&QmModel::setProperty, this, std::tr1::placeholders::_1, std::tr1::placeholders::_2));
		p->setCallback_updateModelValue(std::tr1::bind(&QmModel::updateProperty, this, std::tr1::placeholders::_1, std::tr1::placeholders::_2));

		QmId id = (QmId)p->getValue(QP_PROPERTY_ROLE_ID).toInt();
		debug->info(__FILE__, __LINE__, QString("\t%1").arg(p->getValue(QP_PROPERTY_ROLE_DESCRIPTION).toString()));
		properties->insert(id, p);

		QList<QmId> deps = p->getDependences();
		foreach (QmId d, deps) {
			debug->info(__FILE__, __LINE__, QString("\tdepends on <%1>").arg(d));
			if (!dependencies->contains(d)) {
				QList<QmProperty*> * l = new QList<QmProperty*>;
				dependencies->insert(d, l);
			}
			Q_ASSERT(dependencies->isEmpty() || !dependencies->value(d)->contains(p));
			dependencies->value(d)->append(p);
		}
	}
	return;
}

void
QmModel::setProperty(const QmId id, const QVariant value)
{
	debug->info(__FILE__, __LINE__, QString("setProperty <%1>: %2").arg(id).arg(value.toString()));
	Q_ASSERT(properties->contains(id));
	QmProperty * property = properties->value(id);
	property->setValue(value);
	return;
}

void
QmModel::processInternalQueue(void)
{
	while (!internalQueue->isEmpty()) {
		const QmId * id = &internalQueue->first().first;
		const QVariant * value = &internalQueue->first().second;
		emit propertyChanged(*id, *value);
		if (dependencies->contains(*id)) {
			QList<QmProperty *>* dependentProperties = dependencies->value(*id);
			foreach(QmProperty * property, *dependentProperties) {
				property->updateValue();
			}
		}
		internalQueue->removeFirst();
	}
}

void
QmModel::updateProperty(const QmId id, const QVariant value)
{
	debug->info(__FILE__, __LINE__, QString("updateProperty <%1>: %2").arg(id).arg(value.toString()));
	internalQueue->append(QPair<QmId, QVariant>(id, value));
	if (internalQueue->size() == 1)
		processInternalQueue();
}

QVariant
QmModel::getProperty(const QmId id, const QmPropertyRole role)
{
	Q_ASSERT(properties->contains(id));
	QmProperty * item = properties->value(id);
	return item->getValue(role);
}

QList<QVariant>
QmModel::getPropertyList(const QmId id, const QmPropertyRole role)
{
	Q_ASSERT(properties->contains(id));
	QmProperty * item = properties->value(id);
	return item->getValueList(role);
}

void
QmModel::refresh()
{
	debug->info(__FILE__, __LINE__, QString("Refreshing model..."));
	QHash<QmId, QmProperty*>::iterator i;
	for (i = properties->begin(); i != properties->end(); i++) {
		QmId id = i.key();
		QmProperty * property = i.value();
		debug->info(__FILE__, __LINE__, QString("property is: %1").arg(property->getValue(QP_PROPERTY_ROLE_DESCRIPTION).toString()));
		QVariant value = property->getValue();
		updateProperty(id, value);
	}
}
