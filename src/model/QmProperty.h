/*
 * Copyright (c) 2013, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef QMPROPERTY_H_
#define QMPROPERTY_H_

#include <tr1/functional>
#include <QObject>
#include <QVariant>
#include <properties.h>
#include "QmDebug.h"

	typedef enum {
		QP_PROPERTY_ROLE_RAW,
		QP_PROPERTY_ROLE_DISPLAY,
		QP_PROPERTY_ROLE_DESCRIPTION,
		QP_PROPERTY_ROLE_BRIEF_DESCRIPTION,
		QP_PROPERTY_ROLE_ID,
	} QmPropertyRole;

class QmProperty : public QObject
{
	Q_OBJECT

public:
	QmProperty(const QmId = PROPERTY_INVALID);
	virtual ~QmProperty();

	QList<QmId>					getDependences(void);
	QVariant 					getValue(const QmPropertyRole r = QP_PROPERTY_ROLE_RAW);
	QList<QVariant>				getValueList(const QmPropertyRole r = QP_PROPERTY_ROLE_RAW);
	virtual void 				setValue(const QVariant value = QVariant());
	virtual void 				updateValue(void) {}; /* Is called by model if any
												dependency value was changed */

	/* Property call-backs for dealing with values from model */
	/* Returns value for specified "id" */
	typedef std::tr1::function<QVariant (const QmId, const QmPropertyRole)>	GetModelValueFunction;
	/* Sets value for specified "id" */
	typedef std::tr1::function<void (const QmId, const QVariant)>			SetModelValueFunction;
	/* Notifies model on change of its own value */
	typedef std::tr1::function<void (const QmId, const QVariant)> 			UpdateModelValueFunction;

	void 						setCallback_getModelValue(GetModelValueFunction f) {getModelValue = f;}
	void 						setCallback_setModelValue(SetModelValueFunction f) {setModelValue = f;}
	void 						setCallback_updateModelValue(UpdateModelValueFunction f) {updateModelValue = f;}

protected:
	QVariant 					getOwnValue(void);
	QList<QVariant> 			getOwnValueList(void);
	virtual QVariant			getValueBriefDescription(const QVariant v = QVariant());
	virtual QVariant			getValueDescription(const QVariant v = QVariant());
	virtual QVariant			getValueDisplay(const QVariant v);
	void	 					setOwnValue(const QVariant v);
	void						setOwnValueList(const QList<QVariant> v);

	QString						briefDescription;
	QmDebug* 					debug;
	QList<QmId>					dependences;
	QString						description;
	bool 						filterSameValues;
	QmId						id;
	int 						instance;

	/* Property call-backs for dealing with values from model */
	/* Returns value for specified "id" */
	GetModelValueFunction 		getModelValue;
	/* Sets value for specified "id" */
	SetModelValueFunction 		setModelValue;
	/* Notifies model on change of its own value */
	UpdateModelValueFunction 	updateModelValue;

private:
	bool 						leadingNumberLessThan(const QVariant q1, const QVariant q2);
	QVariant					value;
	QList<QVariant>				valueList;
};

#endif /* QMPROPERTY_H_ */
