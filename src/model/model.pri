INCLUDEPATH	+= $$PWD
DEPENDPATH += $$PWD

SOURCES += $$PWD/QmProperty.cpp \
			$$PWD/QmDebug.cpp \
			$$PWD/QmModel.cpp \
			$$PWD/QmHandler.cpp \
			$$PWD/QmWidget.cpp

HEADERS	+= $$PWD/QmProperty.h \
			$$PWD/QmDebug.h \
			$$PWD/QmModel.h \
			$$PWD/QmHandler.h \
			$$PWD/QmWidget.h

#CONFIG += console
