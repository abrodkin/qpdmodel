/*
 * Copyright (c) 2013, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef QWPUSHBUTTON_H_
#define QWPUSHBUTTON_H_

#include <QPushButton>
#include <QVBoxLayout>

#include <QmModel.h>
#include <QmWidget.h>

class QwPushButton: public QmWidget {
	Q_OBJECT

public:
	QwPushButton(QWidget* parent = 0, QmModel* model = 0, QmId id = PROPERTY_INVALID, bool isTogle = false, bool showText = true);
	virtual ~QwPushButton();

	void			setText(const QString text);
	void			press(const bool value);

private:
	bool			toggle;
	bool			showText;
	QPushButton*	button;
	QVBoxLayout*	layout;

private slots:
	void			pressed(void);
	void			toggled(const bool checked);

signals:
	void			signal_pressed(const bool value);

public slots:
	void			slot_propertyChanged(const QmId name = PROPERTY_INVALID, const QVariant value = QVariant());
};

#endif /* QWPUSHBUTTON_H_ */
