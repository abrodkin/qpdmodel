INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD/QwLabelWithValue.h \
			$$PWD/QwProgressBar.h \
			$$PWD/QwComboBox.h \
			$$PWD/QwLabel.h \
			$$PWD/QwPushButton.h \
			$$PWD/QwSlider.h

SOURCES += $$PWD/QwLabelWithValue.cpp \
			$$PWD/QwProgressBar.cpp \
			$$PWD/QwComboBox.cpp \
			$$PWD/QwLabel.cpp \
			$$PWD/QwPushButton.cpp \
			$$PWD/QwSlider.cpp
