/*
 * Copyright (c) 2013, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "QwPushButton.h"

QwPushButton::QwPushButton(QWidget* parent, QmModel* model, QmId id, bool toggle, bool showText)
	: QmWidget(parent, model, instance)
{
	this->toggle = toggle;
	this->showText = showText;
	this->id = id;
	button = new QPushButton();
	if (toggle) {
		button->setCheckable(true);
		connect(button, SIGNAL(toggled(const bool)), this, SLOT(toggled(const bool)));
	}
	else {
		connect(button, SIGNAL(pressed(void)), this, SLOT(pressed(void)));
	}

	layout = new QVBoxLayout();
	layout->addWidget(button);
	layout->setSpacing(0);
	layout->setContentsMargins(QMargins());
	setLayout(layout);
	valueList = model->getPropertyList(id);
	return;
}

QwPushButton::~QwPushButton() {
	delete button;
	delete layout;
}

void
QwPushButton::slot_propertyChanged(const QmId id, const QVariant v)
{
	if (id == this->id) {
		Q_ASSERT(valueList.size() == 2);
		if (showText) {
			QVariant value = model->getProperty(id, QP_PROPERTY_ROLE_BRIEF_DESCRIPTION);
			button->setText(value.toString());
		}
		if (toggle) {
			button->blockSignals(true);
			if (v == valueList.first())
				button->setChecked(false);
			else
				button->setChecked(true);
			button->blockSignals(false);
		}
	}
	return;
}

void
QwPushButton::pressed(void)
{
	Q_ASSERT(valueList.size() == 2);
	model->setProperty(id, valueList.value(1));
}

void
QwPushButton::toggled(const bool checked)
{
	Q_ASSERT(valueList.size() == 2);
	if (checked) {
		model->setProperty(id, valueList.value(1));
	}
	else {
		model->setProperty(id, valueList.first());
	}
	emit signal_pressed(checked);
}

void
QwPushButton::press(const bool value)
{
	button->setDown(value);
}
