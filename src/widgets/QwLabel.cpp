/*
 * Copyright (c) 2013, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "QwLabel.h"

QwLabel::QwLabel(QWidget* parent, QmModel* model, QmId id, QmPropertyRole role)
	: QmWidget(parent, model, instance)
{
	this->role = role;
	this->id = id;

	label = new QLabel(this);
	label->setWordWrap(true);
	layout = new QVBoxLayout(this);
	layout->addWidget(label);
	layout->setSpacing(0);
	layout->setContentsMargins(QMargins());
	setLayout(layout);
}

QwLabel::~QwLabel() {
	delete label;
	delete layout;
}

void
QwLabel::slot_propertyChanged(const QmId id, const QVariant v)
{
	if (id == this->id) {
		if (role != QP_PROPERTY_ROLE_RAW) {
			label->setText(model->getProperty(id, role).toString());
		}
		else {
			label->setText(v.toString());
		}
	}
	return;
}

void
QwLabel::setText(const QString text)
{
	label->setText(text);
}

void
QwLabel::setAlignment(const Qt::Alignment alignment)
{
	label->setAlignment(alignment);
}
