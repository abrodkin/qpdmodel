/*
 * Copyright (c) 2013, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "QwComboBox.h"

QwComboBox::QwComboBox(QWidget* parent, QmModel* model, QmId id)
	: QmWidget(parent, model)
{
	this->id = id;
	comboBox = new QComboBox();
	connect(comboBox, SIGNAL(activated(const QString)), this, SLOT(activated(const QString)));
	layout = new QVBoxLayout();
	layout->addWidget(comboBox);
	layout->setSpacing(0);
	layout->setContentsMargins(QMargins());
	setLayout(layout);

	value = model->getProperty(id, QP_PROPERTY_ROLE_RAW);
	valueDisplay = model->getProperty(id, QP_PROPERTY_ROLE_DISPLAY);
	valueList = model->getPropertyList(id, QP_PROPERTY_ROLE_RAW);
	valuesDisplay = model->getPropertyList(id, QP_PROPERTY_ROLE_DISPLAY);
	comboBox->clear();
	foreach(QVariant value, valuesDisplay) {
		comboBox->addItem(value.toString());
	}
}

QwComboBox::~QwComboBox() {
	delete comboBox;
	delete layout;
}

void
QwComboBox::slot_propertyChanged(const QmId id, const QVariant v)
{
	if (id == this->id) {
		valueDisplay = model->getProperty(id, QP_PROPERTY_ROLE_DISPLAY);
		comboBox->setCurrentIndex(comboBox->findText(valueDisplay.toString()));
	}
	return;
}

void
QwComboBox::activated(const QString text)
{
	if (text != valueDisplay.toString()) {
		int index = valuesDisplay.indexOf(text);
		model->setProperty(id, valueList.at(index));
	}
}
