/*
 * Copyright (c) 2013, Alexey Brodkin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 * - Names of its contributors may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "QwSlider.h"

QwSlider::QwSlider(QWidget *parent, QmModel* model, QmId id)
	: QmWidget(parent, model)
{
	this->id = id;

	slider = new QSlider(this);
	connect(slider, SIGNAL(valueChanged(const int)), this, SLOT(valueChanged(const int)));
	layout = new QVBoxLayout();
	layout->addWidget(slider);
	layout->setSpacing(0);
	layout->setContentsMargins(QMargins());
	setLayout(layout);
	valueList = model->getPropertyList(id);
	slider->setMinimum(valueList.first().toInt());
	slider->setMaximum(valueList.last().toInt());
}

QwSlider::~QwSlider() {
	delete slider;
	delete layout;
}

void
QwSlider::slot_propertyChanged(const QmId id, const QVariant v)
{
	if (id == this->id) {
		slider->setValue(v.toInt());
	}
	return;
}

void
QwSlider::valueChanged(const int v)
{
	model->setProperty(id, QVariant(v));
}
