#ifndef QPDMODEL_H
#define QPDMODEL_H

#include <QtGui/QWidget>
#include <QVBoxLayout>

#include <QmModel.h>

#include <QwComboBox.h>
#include <QwLabel.h>
#include <QwLabelWithValue.h>
#include <QwProgressBar.h>
#include <QwPushButton.h>
#include <QwSlider.h>

class qpdmodel : public QWidget
{
	Q_OBJECT

public:
	qpdmodel(QWidget *parent = 0);
	~qpdmodel();

private:
	void			showEvent (QShowEvent* event);
	QVBoxLayout*	mainLayout;
	QmModel*		model;
};

#endif // QPDMODEL_H
