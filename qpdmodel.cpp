#include "qpdmodel.h"
#include "properties.h"
#include <QhTest.h>

qpdmodel::qpdmodel(QWidget* parent)
	: QWidget(parent)
{
	qRegisterMetaType<QmId>("Property ID");
	model = new QmModel();
	model->addHandler(new QhTest());

	mainLayout = new QVBoxLayout();
	this->setLayout(mainLayout);

	mainLayout->addWidget(new QwLabel(this, model, PROPERTY_0));
	mainLayout->addWidget(new QwLabelWithValue(this, model, PROPERTY_0));
	mainLayout->addWidget(new QwComboBox(this, model, PROPERTY_0));
	mainLayout->addWidget(new QwSlider(this, model, PROPERTY_0));
	mainLayout->addWidget(new QwProgressBar(this, model, PROPERTY_0));

	mainLayout->addWidget(new QwLabel(this, model, PROPERTY_1));
	mainLayout->addWidget(new QwLabelWithValue(this, model, PROPERTY_1));
	mainLayout->addWidget(new QwComboBox(this, model, PROPERTY_1));
	mainLayout->addWidget(new QwPushButton(this, model, PROPERTY_1, true));
}

qpdmodel::~qpdmodel()
{
	delete mainLayout;
	delete model;
}

void
qpdmodel::showEvent (QShowEvent*)
{
	model->refresh();
}
